/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
public class Main
{
	public static void main(String[] args) {
	    int a = Integer.parseInt (args[0]);
	    int b = Integer.parseInt (args[1]);
	    int c = a+b;
		System.out.printf("The Sum of %d and %d is %d", a,b,c);
	}
}
